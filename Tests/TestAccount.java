import com.company.Account;
import org.junit.Before;
import org.junit.Test;

import java.text.NumberFormat;

import static org.junit.jupiter.api.Assertions.*;

public class TestAccount {

    Account a;
    float amount1;
    float amount2;
    float amount3;
    float commission1;
    float commission2;
    float commission3;
    private final NumberFormat fmt = NumberFormat.getCurrencyInstance();

    @Before
    public void init() {
        a = new Account("Aze", 123, 10000);
        amount1 = -10.5f;
        amount2 = 0;
        amount3 = 1002f;
        commission1 = -1.5f;
        commission2 = 0;
        commission3 = 12f;
    }

    @Test
    public void getAccountNumber() {
        assertEquals(123, a.getAccountNumber());
    }

    @Test
    public void depositTest() {
        assertAll("Deposit Tests should return False, True, True",
            () -> assertFalse(this.a.deposit(amount1)),
            () -> assertTrue(this.a.deposit(amount2)),
            () -> assertTrue(this.a.deposit(amount3))
        );
    }

    @Test
    public void withdrawTest() {
        assertAll("Withdraw Tests should return false, true, true",
                () -> assertFalse(this.a.withdraw(amount1, commission1)),
                () -> assertTrue(this.a.withdraw(amount2, commission2)),
                () -> assertTrue(this.a.withdraw(amount3, commission3))
        );
    }

    @Test
    public void isValidWithdraw() {
        assertAll("isValidWithdraw Tests should return false, true, true",
                () -> assertFalse(this.a.isValidWithdraw(amount1, commission1)),
                () -> assertTrue(this.a.isValidWithdraw(amount2, commission2)),
                () -> assertTrue(this.a.isValidWithdraw(amount3, commission3))
        );
    }

    @Test
    public void getBalanceTest() {
        assertEquals(10000, a.getBalance());
    }

    @Test
    public void addAnualInterestTest() {
        float value = this.a.getBalance() * 1.1f;
        this.a.addAnnualInterest();
        assertEquals(value, this.a.getBalance());
    }

    @Test
    public void transferMoneyTest() {
        Account compteDesti1 = new Account("Owner1", 456, -1);
        Account compteDesti2 = new Account("Owner2", 789, 0);
        Account compteDesti3 = new Account("Owner3", 666, 1);
        assertAll("FFT",
                () -> assertFalse(this.a.transfer(compteDesti1, amount1, commission1)),
                () -> assertEquals(10000, this.a.getBalance()),
                () -> assertEquals(-1, compteDesti1.getBalance()),
                () -> assertFalse(this.a.transfer(compteDesti2, -5, 6)),
                () -> assertEquals(10000, this.a.getBalance()),
                () -> assertEquals(0, compteDesti2.getBalance()),
                () -> assertTrue(this.a.transfer(compteDesti3, amount3, commission3)),
                () -> assertEquals(8986, this.a.getBalance()),
                () -> assertEquals(1003, compteDesti3.getBalance()),
                () -> assertFalse(new Account("Error farming", 777, 0).transfer(compteDesti3, amount3, commission2))
        );
    }

    @Test
    public void toStringTest() {
        assertEquals((123 + "\tAze\t" + fmt.format(10000)), a.toString());
    }

}
