package com.company;

import java.text.NumberFormat;

public class Account {
    private NumberFormat fmt = NumberFormat.getCurrencyInstance();

    private final float annualInterest = 0.1f;  // interest rate of 10%

    private long acctNumber;
    private float balance;
    public final String name;


    public Account (String owner, long account, float initial)
    {
        name = owner;
        acctNumber = account;
        balance = initial;
    }


    public boolean deposit(float amount)
    {
        boolean result = true;
        // Check the amount is a valid amount
        if (amount < 0)
        {
            result = false;
        }
        else
        {
            balance = balance + amount;
        }

        return result;
    }


    public boolean withdraw(float amount, float commission)
    {
        // check parameters
        if (isValidWithdraw(amount, commission))
        {
            amount += commission;
            balance = balance - amount;
        }
        return isValidWithdraw(amount, commission);
    }

    public boolean isValidWithdraw(float amount, float comission)
    {
        //Check if there is enough money in the account
        return amount >= 0 && comission >= 0 && amount <= balance;

    }

    public void addAnnualInterest()
    {
        balance += (balance * annualInterest);

    }

    public boolean transfer(Account compteDesti, float quantitatTransferir, float commission) {
        if (balance >= quantitatTransferir + commission) {
            boolean check = withdraw(quantitatTransferir, commission);
            if (check) {
                compteDesti.deposit(quantitatTransferir);
                return true;
            }
        }
        return false;
    }

    public float getBalance()
    {
        return balance;
    }


    public long getAccountNumber()
    {
        return acctNumber;
    }


    public String toString()
    {
        return (acctNumber + "\t" + name + "\t" + fmt.format(balance));
    }



}
